upsilo.net
==========

.. toctree::
  :hidden:

  profile

:doc:`/profile`

いろいろな記事
--------------

.. toctree::
  :titlesonly:
  :maxdepth: 1

  articles/tween-analytics
  articles/atokx3-multiarch
  articles/sphinx

  過去の記事 <articles>

外部サイト
----------

* `Twitter <https://twitter.com/kim_upsilon>`_
* `はてなダイアリー <http://d.hatena.ne.jp/kim_upsilon/>`_
* :doc:`その他 </~upsilon/profile>`

