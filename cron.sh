#!/bin/sh

cd `dirname $0`
git -q pull
sphinx-build -q -b html -d _build/doctrees . _build/html/
