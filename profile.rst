プロフィール
============

kim_upsilon
-----------

Web上で活動する際に使用してゐる名前。最近Twitterでしか見なくなった気がする。

.. toctree::
  :maxdepth: 1

  /~upsilon/profile
  連絡先 </~upsilon/contact>

ykimura
-------

実名で活動する際にkim_upsilonと明示的に区別するための名前。

.. toctree::
  :maxdepth: 1

  /~ykimura/profile
  連絡先 </~ykimura/contact>

