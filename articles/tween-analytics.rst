Tweenのトラッキングコードに関する検証
=====================================

(2012-06-26)

環境など
--------

ソースコード
  git://github.com/opentween/OpenTween.git より取得。

  ``git checkout Tween_svn-last`` によりオープンソース時代の Tween 最終版をチェックアウトした。

ビルド環境
  Visual Studio 2010

基本ソフトウェア
  Microsoft Windows XP Professional SP3

共通言語基盤
  Microsoft .NET Framework Version 4.0

ネットワークアナライザ
  Wireshark 1.8.0

参考
----

@kim_upsilon のアカウントIDは `40480664 <https://api.twitter.com/1/users/show/40480664.xml>`_

画面解像度は 1600x833 (32bit)

Flashのバージョンは 11.3 r300

ソースコードに含まれるAnalytics関係のコード: https://bitbucket.org/opentween/opentween/changeset/4c406a1e4a8f

はじまり
--------

えー、私が今から淡々と URL を貼り付けていきますので、皆さんでクエリーの意味を推測して下さい。

そしたら私が適当な時に喋り始めますので、続けてツッコミを入れていただきたい。

はい円楽さん早かった。

Tween初回起動から認証直後まで
-----------------------------

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=397637033&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=54020666&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684385.1340684385.1340651985.1;&
    utmu=q~&
    utmt=event&
    utme=5(post*authenticate)

設定画面を閉じてメインウィンドウが表示されるまで
------------------------------------------------

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1415215008&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=718503285&
    utmr=-&
    utmp=/home_timeline&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340684553.1340652153.2;&
    utmu=q~

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1403450209&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=516348951&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652153.1340652153.3;&
    utmu=q~&
    utmt=event&
    utme=5(post*start)

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=428201238&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1593817395&
    utmr=-&
    utmp=/userstream&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652153.1340652154.4;&
    utmu=q~


投稿してみる
------------

「@kim_upslion test」 ::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1607871625&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=78697803&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652154.1340652577.5;&
    utmu=q~&
    utmt=event&
    utme=5(post*status)

「@kim_upslion てすと」 ::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=280939980&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1647624739&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652577.1340652680.6;&
    utmu=q~&
    utmt=event&
    utme=5(post*status)

さっき投稿したツイートを削除
----------------------------

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=664051752&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1973885182&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652680.1340652733.7;&
    utmu=q~&
    utmt=event&
    utme=5(post*destroy)

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=2146101365&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=954906238&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652733.1340652734.8;&
    utmu=q~&
    utmt=event&
    utme=5(post*destroy)

ふぁぼ
------

http://twitter.com/Flasub/status/217339782383153154 をふぁぼった

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1556571365&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1242318899&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652734.1340652844.9;&
    utmu=q~&
    utmt=event&
    utme=5(post*favorites)

プロフィール
------------

@firefox (`2142731 <https://api.twitter.com/1/users/show/2142731.xml>`_) のプロフィールを開いた

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=496539704&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1142776487&
    utmr=-&
    utmp=/showuser&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652844.1340652909.10;&
    utmu=q~

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1249569466&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&utmhid=1837782710&
    utmr=-&
    utmp=/user_profile&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652909.1340652911.11;&
    utmu=q~

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1978822330&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=346180621&
    utmr=-&
    utmp=/friendships&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652911.1340652911.12;&
    utmu=q~

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=594429545&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1023923712&
    utmr=-&
    utmp=/home_timeline&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340652911.1340652924.13;&
    utmu=q~

タブ切り替え
------------

Reply ::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1017774690&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=206536023&
    utmr=-&
    utmp=/mentions&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653096.1340653193.16;&
    utmu=q~

Direct ::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1066191117&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=645167436&
    utmr=-&
    utmp=/direct_messages&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653193.1340653231.17;&
    utmu=q~

Favorites ::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=509457825&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=552529614&
    utmr=-&
    utmp=/favorites&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653231.1340653258.18;&
    utmu=q~

タブ作成
--------

リスト @kim_upsilon/udon2012 のタブを作成

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1006873259&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=1763750705&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653258.1340653309.19;&
    utmu=q~&
    utmt=event&
    utme=5(post*add_tab)

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=1642976844&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=2037821178&
    utmr=-&
    utmp=/lists&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653309.1340653309.20;&
    utmu=q~

タブ削除
--------

さっき作ったタブを削除

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=2027681401&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=598225969&
    utmr=0&
    utmp=/&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653309.1340653391.21;&
    utmu=q~&
    utmt=event&
    utme=5(post*remove_tab)

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=719748093&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=262462735&
    utmr=-&
    utmp=/favorites&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653391.1340653393.22;&
    utmu=q~

リンクを開いてみる
------------------

たまたま目に入った http://t.co/XiWkRu07 (=> http://mattn.kaoriya.net/software/lang/c/20120625184100.htm) を開いた

::

  http://www.google-analytics.com/__utm.gif?
    utmwv=5.1.5&
    utms=1&
    utmn=783181392&
    utmhn=apps.tweenapp.org&
    utmcs=shift_jis&
    utmsr=1600x833&
    utmsc=32-bit&
    utmul=ja-JP&
    utmje=1&
    utmfl=10.0 r32&
    utmhid=216423606&
    utmr=-&
    utmp=/open_url&
    utmac=UA-4618605-5&
    utmcc=__utma=211246021.40480664.1340684553.1340653605.1340653706.24;&
    utmu=q~

リクエスト全体
--------------

:: 

  GET /__utm.gif?utmwv=5.1.5&utms=1&utmn=397637033&utmhn=apps.tweenapp.org&utmcs=shift_jis&utmsr=1600x833&utmsc=32-bit&utmul=ja-JP&utmje=1&utmfl=10.0%20r32&utmhid=54020666&utmr=0&utmp=%2F&utmac=UA-4618605-5&utmcc=__utma%3D211246021.40480664.1340684385.1340684385.1340651985.1%3B&utmu=q~&utmt=event&utme=5%28post%2Aauthenticate%29 HTTP/1.1
  Accept: */*
  Referer: http://apps.tweenapp.org/foo.html
  Accept-Language: ja-JP
  User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; MALC)
  Accept-Encoding: gzip, deflate
  Host: www.google-analytics.com
  Connection: Keep-Alive

さいごに
--------

私は http://apps.tweenapp.org/foo.html を開いた覚えは一切ありません。

