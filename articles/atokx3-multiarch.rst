:Date: 2012-06-16

ATOK X3 for LinuxをDebian(amd64)にインストールしてみた（Multiarchサポート使用）
===============================================================================

(2012-06-16)

みなさん ATOK 使ってますか？ ATOK の文語モードだと歴史的仮名遣ひな漢字仮名交じり文もすらすら変換できて入力が捗りますよ。

今回はそんな便利な ATOK の Linux 版である `ATOK X3 for Linux <http://www.justsystems.com/jp/products/atok_linux/>`_ を Debian (**amd64**) にインストールしてみます。

Debian とか Ubuntu の amd64 環境で ATOK X3 をインストールする手順は既に他所で公開されてゐますが、最近 Debian の sid で **Multiarch** サポートが使へるやうになったので今回はこれを活用して ATOK をインストールしてみます。Multiarch を使ふことによって ``ia32-libs`` パッケージのインストールや i386 用の chroot 環境を整備する必要も無くなります。

Multiarch って何？って人は http://wiki.debian.org/Multiarch をひたすら読んで下さい。

なほ、Multiarch は dpkg 1.16.2 から追加された機能なので、それ以前のバージョンを使用してゐる環境ではここで解説する手順を実行することはできません。記事公開の時点 (2012-06-16) では wheezy (testing) 以降であれば Multiarch が利用できると思ひます。

では始めますよ。

Multiarchの設定
---------------

この辺は http://wiki.debian.org/Multiarch/HOWTO を見ながら進めます。

1. i386 アーキテクチャを追加します ::

   $ sudo dpkg --add-architecture i386

2. パッケージの一覧を更新します ::

   $ sudo aptitude update

ATOK X3 for Linuxのインストール
-------------------------------

言ひ忘れましたが ATOK X3 for Linux は購入済みであることを前提に話を進めます。

3. ``atokx3.tar.gz`` を展開する ::

   $ tar zxfv atokx3.tar.gz
   $ cd ATOKX3/

4. さらに ATOKX3 ディレクトリ内に `dpkg 1.15.8 対応 deb パッケージ <http://support.justsystems.com/faq/1032/app/servlet/qadoc?QID=048707>`_ を展開する ::

   $ tar zxfv atokx3dpkg1158_2.tar.gz

5. 依存するライブラリなどを予めインストールする ::

   $ sudo aptitude install libatk1.0-0:i386 libc6:i386 libgcc1:i386 libglib2.0-0:i386 libgtk2.0-0:i386 libpango1.0-0:i386 libstdc++6:i386 libx11-6:i386 libice6:i386 libsm6:i386 libpam0g:i386 libwrap0:i386 libxml2:i386 zlib1g:i386 libxt6:i386

6. ATOK 及び IIIMF のパッケージをインストールする ::

   $ sudo dpkg -i bin/deb/ATOK/atokx_20.0-1.0.0_i386.deb bin/deb/IIIMF/iiimf-client-lib_3104-js3_i386.deb bin/deb/IIIMF/iiimf-protocol-lib_3104-js3_i386.deb bin/deb/IIIMF/iiimf-server_3104-js3_i386.deb bin/deb/IIIMF/iiimf-x_3104-js3_i386.deb

さてここで問題が起きます。上記の手順で i386 向けのパッケージはすんなりインストールできましたが、 ``iiimf-gtk`` は amd64 版のものを入れなければなりません。しかし、iiimf-gtk の amd64 版 deb パッケージは用意されてゐません。しかも、tarball の iiimf-gtk は ``/usr/lib64/gtk-2.0/immodules/`` に im-iiim.so がインストールされますが、Multiarch な Debian では ``/usr/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/immodules/`` に置かれるべきです。大変ですねー。

といふわけで何とかして iiimf-gtk を使へるやうにします。


im-iiim.soをなんとかして入れる
------------------------------

いろいろ考へた結果、im-iiim.so は ``/usr/local/lib/gtk-2.0/immodules/`` ディレクトリを作ってその中に入れることにしました。dpkg で管理されてゐないファイルを /usr/lib 以下に入れるのはちょっと気持ち悪いですよね。

では続き。

7. 適当なディレクトリに iiimf-gtk-64-trunk_r3104-js1.x86_64.tar.gz を展開する ::

   $ tar zxfv bin/tarball/IIIMF/iiimf-gtk-64-trunk_r3104-js1.x86_64.tar.gz

8. インストール先のディレクトリを作成する ::

   $ sudo mkdir /usr/local/lib/gtk-2.0/immodules

9. im-iiim.so と mo ファイルをコピーする ::

   $ sudo cp ./usr/lib64/gtk-2.0/immodules/im-iiim.so /usr/local/lib/gtk-2.0/immodules
   $ sudo cp -Rv ./usr/share/locale /usr/local/share/

10. libiiimcf.so.3 と libiiimp.so.1 も必要なのでこれも取り出してコピーする ::

    $ tar zxfv bin/tarball/IIIMF/iiimf-client-lib-64-trunk_r3104-js1.x86_64.tar.gz
    $ tar zxfv bin/tarball/IIIMF/iiimf-protocol-lib-64-trunk_r3104-js1.x86_64.tar.gz
    $ sudo cp ./usr/lib64/libiiimcf.so* ./usr/lib64/libiiimp.so* /usr/local/lib/

11. いまいち気に食はないけど、かうしないとうまく動かなかったので妥協してごにょごにょする ::

    $ sudo ln -s /usr/local/lib/gtk-2.0/immodules/im-iiim.so /usr/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/immodules/
    $ /usr/lib/x86_64-linux-gnu/libgtk2.0-0/gtk-query-immodules-2.0 | sudo tee /usr/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/gtk.immodules

xinputrcの設定とか
------------------

この辺はよく分からないので見ての通りの適当さ加減。

12. もう何やってんだか全然分かんないけど動いちゃったしこれでいいよね ::

    $ sudo rm /etc/X11/xinit/xinputrc
    $ sudo ln -s /opt/atokx3/bin/atokx3start.sh /etc/X11/xinit/xinputrc

ATOKのアップデートモジュールを適用する
--------------------------------------

仕上げ(?)に ATOK のアップデートモジュールを適用していきます。アップデートモジュールは http://support.justsystems.com/faq/1032/app/servlet/qasearchtop?MAIN=002003003001001 で公開されてゐるので適宜ダウンロードして下さい。

13. ATOK X3 for Linux アップデートモジュール (2009.03.03) ::

    $ tar zxfv ~/downloads/atokx3up2.tar.gz
    $ sudo tar zxfv ./atokx3up2/bin/ATOK/atokxup-20.0-3.0.0.i386.tar.gz -C /

14. ATOK X3 for Linux Ubuntu 9.04(GTK+ 2.16) 対応モジュール (2009.06.04) ::

    $ sudo tar zxfv ~/downloads/atokx3gtk216.tar.gz -C /

15. ATOK X3 for Linux用郵便番号辞書 (2011.11.24) ::

    $ sudo tar zxfv ~/downloads/a20y1111lx.tgz -C /

めでたしめでたし
----------------

スクリーンショット撮るのめんどくさくなった。ごめん。

さいごに
--------

`大統一Debian勉強会 <http://gum.debian.or.jp/>`_ が来週の土曜日にあるらしいですねー。どうやら Debian Multiarch Support についての発表もあるらしいですねー。

京都まで行く気満々だったけど都合が付かなくなってしょんぼりしながらこの記事書きました。まる。

