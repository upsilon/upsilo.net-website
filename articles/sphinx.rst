:Date: 2012-05-12

upsilo.net を Sphinx で作り直しました
=====================================

(2012-05-12)

気まぐれで upsilo.net のコンテンツを全面的に Sphinx で作り直しました。

とはいへ全部置き換へるのは流石にめんどくさいので一部の (古い) コンテンツはそのままです。
URL 構造は極力変はらないやうに配慮したのでリンク切れの心配はたぶん無いと思ひます。

作り直したついでにビルド前のソースを Bitbucket で公開してゐます。Sphinx なので記述言語は reStructuredText です。

https://bitbucket.org/upsilon/upsilo.net-website

誤植とか見つけたら遠慮無くPull Request送って下さい（丸投げ

ちなみに、Sphinx へ移行したのは 4 ヶ月前とかの話だった気がします。
