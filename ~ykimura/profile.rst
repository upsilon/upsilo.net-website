ykimura まとめ
==============
自己紹介代はりのリンク集。下記のページはすべて同一人物による仕業です。

ykimura への連絡手段については :doc:`連絡先 <contact>` を参照して下さい。

OpenPNE 関連
------------
OpenPNE Issue Tracking System (Youichi Kimura)
  http://redmine.openpne.jp/users/41

