kim_upsilon まとめ
==================
自己紹介代はりのリンク集。下記のページはすべて同一人物による仕業です。

kim_upsilon への連絡手段については :doc:`連絡先 <contact>` を参照して下さい。

Twitter 関連
------------
Twitter (kim_upsilon)
  https://twitter.com/kim_upsilon
Twilog (kim_upsilon)
  http://twilog.org/kim_upsilon
Favolog (kim_upsilon)
  http://favolog.org/kim_upsilon
Favstar (kim_upsilon)
  http://favstar.fm/users/kim_upsilon/recent
Twitpic (kim_upsilon)
  https://twitpic.com/photos/kim_upsilon

オープンソース全般
------------------
GitHub (upsilon)
  https://github.com/upsilon
Bitbucket (upsilon)
  https://bitbucket.org/upsilon
SourceForge.JP (upsilon)
  https://sourceforge.jp/users/upsilon/
coderwall (upsilon)
  http://coderwall.com/upsilon

OpenPNE 関連
------------
OpenPNE Plugins (upsilon)
  http://plugins.openpne.jp/member/20
OpenPNE公式SNS (upsilon)
  https://sns.openpne.jp/member/7169

OpenStreetMap 関連
------------------
OpenStreetMap (kim-upsilon)
  http://www.openstreetmap.org/user/kim-upsilon
OpenStreetMap Wiki (kim_upsilon)
  http://wiki.openstreetmap.org/wiki/User:kim_upsilon

その他
------
はてなダイアリー (kim_upsilon)
  https://d.hatena.ne.jp/kim_upsilon/
ブクログ (kim_upsilon)
  https://booklog.jp/users/kim-upsilon

